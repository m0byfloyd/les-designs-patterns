DOCKER_COMPOSE  = docker compose

EXEC_PHP        = $(DOCKER_COMPOSE) run --rm php

SYMFONY         = $(EXEC_PHP) bin/console
COMPOSER        = $(EXEC_PHP) composer

##Setup
install: pull start vendor db ## Install everything

##
##Docker commands
build: ## Build docker containers
	touch docker/php/conf/history
	$(DOCKER_COMPOSE) build

pull: ## Pull docker containers from the registry
	touch docker/php/conf/history
	$(DOCKER_COMPOSE) pull

start: ## Start docker containers
	$(DOCKER_COMPOSE) up -d --remove-orphans --force-recreate

start-debug: ## Start docker containers with xdebug enabled
	XDEBUG_MODE=debug $(DOCKER_COMPOSE) up -d --remove-orphans --force-recreate

stop: ## Stop docker containers
	$(DOCKER_COMPOSE) down

vendor: ## Install composer dependencies
	$(COMPOSER) install

restart: stop start ## Restart docker containers

kill: ## Kill and removes docker containers (with volumes)
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

##
##Symfony commands
php: ## Shell into the php container
	$(DOCKER_COMPOSE) run --rm php bash

php-debug: ## Shell into the php container with xdebug enabled
	XDEBUG_MODE=debug $(DOCKER_COMPOSE) run --rm php bash

cc: ## Clear the Symfony cache
	$(SYMFONY) cache:clear

db: vendor ## Reset the database
	$(SYMFONY) doctrine:database:drop --force
	$(SYMFONY) doctrine:database:create
	$(SYMFONY) doctrine:schema:update --force
	$(SYMFONY) doctrine:fixtures:load --no-interaction --append

##
##Quality tools
cs: ## Run PHP CS Fixer
	$(EXEC_PHP) ./vendor/bin/php-cs-fixer fix --verbose --diff --show-progress=dots

phpstan: ## Run phpstan
	$(EXEC_PHP) ./vendor/bin/phpstan

test: ## Run the tests
	$(EXEC_PHP) bin/phpunit

test-group: ## Run tests for one group → Type: group=<group_name> make test-group
	$(EXEC_PHP) bin/phpunit --group=${group}

node:
	$(DOCKER_COMPOSE) run --rm node bash

.PHONY: build kill install start stop db migrate migration vendor restart php pull cs test

##
##Help
help: ## Show this help message
	@grep -E '(^[0-9a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.DEFAULT_GOAL := help
