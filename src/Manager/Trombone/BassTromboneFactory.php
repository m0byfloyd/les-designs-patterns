<?php

namespace App\Manager\Trombone;

use App\Entity\Instrument;

class BassTromboneFactory extends TromboneFactory
{
    public static function createInstrument($arrayArgs): Instrument
    {
        $arrayArgs = array_merge($arrayArgs, [
            'numberOfValves'=>2,
            'type'=> 'bass',
            'canSlide'=>true,
            'numberOfPistons'=>0
        ]);

        return parent::createInstrument($arrayArgs);

    }

}
