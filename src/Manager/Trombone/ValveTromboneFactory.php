<?php

namespace App\Manager\Trombone;

use App\Entity\Instrument;

class ValveTromboneFactory extends TromboneFactory
{

    public static function createInstrument($arrayArgs): Instrument
    {
        $arrayArgs = array_merge($arrayArgs, [
            'numberOfValves'=>0,
            'type'=> 'valve',
            'canSlide'=>false,
            'numberOfPistons'=>3
        ]);

        return parent::createInstrument($arrayArgs);
    }

}
