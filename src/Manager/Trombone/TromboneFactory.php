<?php

namespace App\Manager\Trombone;

use App\Controller\Helper\FactoryHelper;
use App\Entity\Instrument;
use App\Entity\Trombone;
use App\Manager\InstrumentFactory;

abstract class TromboneFactory extends InstrumentFactory
{
    public static function createInstrument($arrayArgs): Instrument{
        return FactoryHelper::hydrateObject(Trombone::class,$arrayArgs);
    }
}
