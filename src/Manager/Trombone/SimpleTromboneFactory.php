<?php

namespace App\Manager\Trombone;

use App\Entity\Instrument;

class SimpleTromboneFactory extends TromboneFactory
{
    public static function createInstrument($arrayArgs): Instrument
    {
        $arrayArgs = array_merge($arrayArgs, [
            'numberOfValves'=>0,
            'type'=> 'simple',
            'canSlide'=>true,
            'numberOfPistons'=>0
        ]);

        return parent::createInstrument($arrayArgs);
    }

}
