<?php

namespace App\Manager;

use App\Entity\Instrument;

abstract class InstrumentFactory
{
    public static function createInstrument($arrayArgs): Instrument
    {
    }

    public static function testInstrument($instrument): void
    {
        echo $instrument->play('test') . '<br/>';
    }

    public static function createInstrumentAndTest($arrayArgs): Instrument
    {
        $instrument = static::createInstrument($arrayArgs);
        static::testInstrument($instrument);

        return $instrument;
    }

}
