<?php

namespace App\Manager\Trumpet;

use App\Controller\Helper\FactoryHelper;
use App\Entity\Trumpet;
use App\Manager\InstrumentFactory;

class TrumpetFactory extends InstrumentFactory
{
    public static function createInstrument($arrayArgs): Trumpet{
        return FactoryHelper::hydrateObject(Trumpet::class,$arrayArgs);

    }
}
