<?php

namespace App\Controller\Helper;

class FactoryHelper
{
    public static function hydrateObject($objectClass, $arrayArgs) {
        $object = new $objectClass();
        $objectMethods = get_class_methods($object);

        foreach ($objectMethods as $method) {
            preg_match(' /^(set)(.*?)$/i', $method, $results);

            if (!empty($results)) {
                $argument = $results[2]  ?? '';
                $argument = strtolower(substr($argument, 0, 1)) . substr($argument, 1);
                $object->$method($arrayArgs[$argument]);
            }

        }

        return $object;
    }

}
