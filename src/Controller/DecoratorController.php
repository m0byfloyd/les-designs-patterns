<?php

namespace App\Controller;

use App\DP\Decorator\sauce\IngredientSauceAlgerienne;
use App\DP\Decorator\sauce\IngredientSauceBarbecue;
use App\DP\Decorator\supplement\IngredientSupplementBoursin;
use App\DP\Decorator\supplement\IngredientSupplementCheddarGratine;
use App\DP\Decorator\Taco;
use App\DP\Decorator\viande\IngredientCordonBleu;
use App\DP\Decorator\viande\IngredientMerguez;
use App\DP\Decorator\viande\IngredientPouletMarine;
use App\DP\Decorator\viande\IngredientViandeHachee;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/decorator', name: 'app_decorator')]
class DecoratorController extends AbstractController
{
    #[NoReturn] #[Route('/', name: 'app_decorator_basic', methods: ['GET'])]
    public function index(): Response
    {
        // Début de la préparation
        $tacoMorgane = new Taco();
        $tacoMorgane = new IngredientCordonBleu($tacoMorgane);
        $tacoMorgane = new IngredientSauceAlgerienne($tacoMorgane);

        $tacoPablo = new IngredientMerguez(new IngredientCordonBleu(new IngredientSauceBarbecue(new Taco())));
        $tacoBob = new IngredientViandeHachee(new IngredientMerguez(new IngredientCordonBleu(new IngredientSauceBarbecue(new Taco()))));
        $tacoJo = new IngredientSauceAlgerienne(new IngredientPouletMarine(new IngredientPouletMarine(new Taco())));

        // On passe les tacos au chef supplément
        $tacoJo = new IngredientSupplementBoursin($tacoJo);

        $tacoBob = new IngredientSupplementCheddarGratine($tacoBob);
        $tacoBob = new IngredientSupplementBoursin($tacoBob);

        // Livraison puis dégustation
        $sacOtacos = [
            'Morgane' => $tacoMorgane,
            'Pablo' => $tacoPablo,
            'Jo' => $tacoJo,
            'Bob' => $tacoBob
        ];

        return $this->render('decorator/index.html.twig', ['sacOtacos' => $sacOtacos]);
    }
}
