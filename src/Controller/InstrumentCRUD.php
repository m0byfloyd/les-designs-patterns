<?php

namespace App\Controller;

use App\Repository\TromboneRepository;
use App\Repository\TrumpetRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/instrument')]
class InstrumentCRUD extends AbstractController
{

    #[Route('/', name: 'instrument_index', methods: ['GET'])]
    public function index(TromboneRepository $tromboneRepository, TrumpetRepository $trumpetRepository): Response
    {

        echo 'Les instruments créés : <br/><br/>';

        $instruments = array_merge($trumpetRepository->findAll(), $tromboneRepository->findAll());

        if (count($instruments) === 0) {
            echo 'Y a que dalle frr';
            return new Response();
        }

        echo 'Y a ' . count($instruments) . ' instruments<br/>';

        foreach ($instruments as $instrument) {
            echo $instrument->play() . '<br/>';
        }

        return new Response();

    }

}
