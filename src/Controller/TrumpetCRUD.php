<?php

namespace App\Controller;

use App\Manager\Trumpet\TrumpetFactory;
use App\Repository\TromboneRepository;
use App\Repository\TrumpetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/trumpet')]
class TrumpetCRUD extends AbstractController
{

    public function __construct(
        private readonly EntityManagerInterface $entityManager)
    {
    }

    #[NoReturn] #[Route('/', name: 'trumpet_index', methods: ['GET'])]
    public function index(TrumpetRepository $trumpetRepository)
    {
        dd($trumpetRepository->findAll());
    }

    /**
     * @throws OptimisticLockException
     * @throws Exception
     */
    #[Route('/new', name: 'trumpet_new', methods: ['GET'])]
    public function new(): Response
    {

        $serialNumber = 0;

        for ($i = 0; $i < 10; $i++) {

            $trumpet = (new TrumpetFactory())->createInstrumentAndTest([
                'tromboneSlideLength' => random_int(400, 4985),
                'bellPerimeter' => random_int(456, 987),
                'brand' => random_bytes(6),
                'weight' => random_int(10, 100),
                'instrumentKey' => 'B',
                'material' => 'copper',
                'serialNumber' => $serialNumber,
                'numberOfPistons' => 3
            ]);

            $this->entityManager->persist($trumpet);
            $this->entityManager->flush();
        }

        return new Response('Waw bravo');
    }
}
