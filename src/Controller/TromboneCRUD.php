<?php

namespace App\Controller;

use App\Manager\Trombone\BassTromboneFactory;
use App\Manager\Trombone\SimpleTromboneFactory;
use App\Manager\Trombone\TromboneFactory;
use App\Manager\Trombone\ValveTromboneFactory;
use App\Repository\TromboneRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/trombone')]
class TromboneCRUD extends AbstractController
{

    public function __construct(
        private readonly EntityManagerInterface $entityManager)
    {
    }

    #[NoReturn] #[Route('/', name: 'trombone_index', methods: ['GET'])]
    public function index(TromboneRepository $tromboneRepository)
    {
        dd($tromboneRepository->findAll());
    }

    /**
     * @throws OptimisticLockException
     * @throws Exception
     */
    #[Route('/new', name: 'trombone_new', methods: ['GET'])]
    public function new(): Response
    {

        $serialNumber = 0;

        for ($i = 0; $i < 10; $i++) {
            $types = [BassTromboneFactory::class, SimpleTromboneFactory::class, ValveTromboneFactory::class];

            $randomType = $types[random_int(0, 2)];

            $trombone = $randomType::createInstrumentAndTest([
                'tromboneSlideLength' => random_int(400, 4985),
                'bellPerimeter' => random_int(456, 987),
                'brand' => random_bytes(6),
                'weight' => random_int(10, 100),
                'instrumentKey' => 'D',
                'material' => 'copper',
                'serialNumber' => $serialNumber
            ]);

            $this->entityManager->persist($trombone);
            $this->entityManager->flush();
        }

        return new Response('Waw bravo');
    }
}
