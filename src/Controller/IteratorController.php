<?php

namespace App\Controller;

use App\Collection\GrosCollection;
use App\Collection\menuByDayCollection;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/iterator')]
class IteratorController extends AbstractController
{
    /**
     * @throws Exception
     */
    #[Route('/{menuSort}/{grosSort}', name: 'app_iterator' )]
    public function index(string $menuSort = 'forward', string $grosSort = 'forward'): Response
    {
        // On déclare nos deux collections
        $grosCollectionForHeader = new GrosCollection(['BEN', 'BOB', 'FIFI', 'JONATHAN', 'TIM', 'VIANNEY']);
        $menusByDayCollection = new menuByDayCollection('./gros.csv');

        // On choisit l'itérateur qui convient pour la lecture des lignes de menus par jour
        $menuIterator = $menuSort === 'backward'
            ? $menusByDayCollection->getBackwardIterator()
            : $menusByDayCollection->getIterator();

        // On choisit l'itérateur qui convient pour la lecture des header de gros
        $grosHeadIterator = $grosSort === 'backward'
            ? $grosCollectionForHeader->getBackwardIterator()
            : $grosCollectionForHeader->getIterator();

        // Si on lit les gros dans l'ordre non alphabétique alors on doit retourner les lignes des menus dans l'autre sens
        if ($grosSort === 'backward') {
            $menuIterator = $menusByDayCollection->getReversedRowIterator($menuSort === 'backward');
        }

        return $this->render('iterator/index.html.twig', [
            'grosList' => $grosHeadIterator,
            'monthMenus' => $menuIterator,
            'menuSort'=> $menuSort,
            'grosSort'=> $grosSort,
        ]);
    }
}
