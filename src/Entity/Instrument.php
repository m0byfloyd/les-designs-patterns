<?php

namespace App\Entity;

interface Instrument
{
    public function play($note);

    public function getId(): ?int;

    public function getBrand(): ?string;

    public function getWeight(): ?int;

    public function getInstrumentKey(): ?string;

    public function getMaterial(): ?string;

    public function getSerialNumber(): ?int;

    public function setBrand(string $brand): self;

    public function setWeight(int $weight): self;

    public function setInstrumentKey(string $instrumentKey): self;

    public function setMaterial(string $material): self;

    public function setSerialNumber(int $serialNumber): self;
}
