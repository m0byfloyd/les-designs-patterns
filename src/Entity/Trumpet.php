<?php

namespace App\Entity;

use App\Repository\TrumpetRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TrumpetRepository::class)]
class Trumpet implements Instrument
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $bellPerimeter = null;

    #[ORM\Column(length: 60)]
    private ?string $brand = null;

    #[ORM\Column]
    private ?int $weight = null;

    #[ORM\Column(length: 3)]
    private ?string $instrumentKey = null;

    #[ORM\Column(length: 255)]
    private ?string $material = null;

    #[ORM\Column]
    private ?int $serialNumber = null;

    #[ORM\Column]
    private ?int $numberOfPistons = null;

    /**
     * @return int|null
     */
    public function getBellPerimeter(): ?int
    {
        return $this->bellPerimeter;
    }

    /**
     * @param int|null $bellPerimeter
     */
    public function setBellPerimeter(?int $bellPerimeter): void
    {
        $this->bellPerimeter = $bellPerimeter;
    }

    /**
     * @return string|null
     */
    public function getBrand(): ?string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     * @return Trumpet
     */
    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInstrumentKey(): ?string
    {
        return $this->instrumentKey;
    }

    /**
     * @param string $instrumentKey
     * @return Trumpet
     */
    public function setInstrumentKey(string $instrumentKey): self
    {
        $this->instrumentKey = $instrumentKey;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMaterial(): ?string
    {
        return $this->material;
    }

    /**
     * @param string $material
     * @return Trumpet
     */
    public function setMaterial(string $material): self
    {
        $this->material = $material;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSerialNumber(): ?int
    {
        return $this->serialNumber;
    }

    /**
     * @param int $serialNumber
     * @return Trumpet
     */
    public function setSerialNumber(int $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getNumberOfPistons(): ?int
    {
        return $this->numberOfPistons;
    }

    /**
     * @param int|null $numberOfPistons
     */
    public function setNumberOfPistons(?int $numberOfPistons): void
    {
        $this->numberOfPistons = $numberOfPistons;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    function play($note = 'do'): string
    {
        return 'Trompette : ' . $note;
    }
}
