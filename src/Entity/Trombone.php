<?php

namespace App\Entity;

use App\Repository\TromboneRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TromboneRepository::class)]
class Trombone implements Instrument
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $tromboneSlideLength = null;

    #[ORM\Column]
    private ?int $bellPerimeter = null;

    #[ORM\Column(length: 60)]
    private ?string $brand = null;

    #[ORM\Column]
    private ?int $weight = null;

    #[ORM\Column(length: 3)]
    private ?string $instrumentKey = null;

    #[ORM\Column(length: 255)]
    private ?string $material = null;

    #[ORM\Column]
    private ?int $numberOfValves = null;

    #[ORM\Column]
    private ?int $serialNumber = null;

    #[ORM\Column(length: 20)]
    private ?string $type = null;

    #[ORM\Column]
    private ?int $numberOfPistons = null;

    #[ORM\Column]
    private ?bool $canSlide = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTromboneSlideLength(): ?int
    {
        return $this->tromboneSlideLength;
    }

    public function setTromboneSlideLength(int $tromboneSlideLength): self
    {
        $this->tromboneSlideLength = $tromboneSlideLength;

        return $this;
    }

    public function getBellPerimeter(): ?int
    {
        return $this->bellPerimeter;
    }

    public function setBellPerimeter(int $bellPerimeter): self
    {
        $this->bellPerimeter = $bellPerimeter;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getInstrumentKey(): ?string
    {
        return $this->instrumentKey;
    }

    public function setInstrumentKey(string $instrumentKey): self
    {
        $this->instrumentKey = $instrumentKey;

        return $this;
    }

    public function getMaterial(): ?string
    {
        return $this->material;
    }

    public function setMaterial(string $material): self
    {
        $this->material = $material;

        return $this;
    }

    public function getNumberOfValves(): ?int
    {
        return $this->numberOfValves;
    }

    public function setNumberOfValves(int $numberOfValves): self
    {
        $this->numberOfValves = $numberOfValves;

        return $this;
    }

    public function getSerialNumber(): ?int
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(int $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getNumberOfPistons(): ?int
    {
        return $this->numberOfPistons;
    }

    public function setNumberOfPistons(int $numberOfPistons): self
    {
        $this->numberOfPistons = $numberOfPistons;

        return $this;
    }

    public function isCanSlide(): ?bool
    {
        return $this->canSlide;
    }

    public function setCanSlide(bool $canSlide): self
    {
        $this->canSlide = $canSlide;

        return $this;
    }

    public function play($note = 'fa'): string
    {
        return 'Trombone : ' .$note;
    }
}
