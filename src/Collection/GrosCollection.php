<?php

namespace App\Collection;

use App\Iterators\BackwardIterator;
use App\Iterators\ForwardIterator;

class GrosCollection implements \IteratorAggregate, \Countable
{
    public function __construct(
        private readonly array $items = []
    )
    {
    }

    public function getItems(): array
    {
        return $this->items;
    }


    // J'implémente countable et créé la fonction, je vais en avoir besoin dans un des itérateurs que je déclare pour ma collection.
    public function count(): int
    {
        return count($this->items);
    }

    // Je déclare les itérateurs que ma collection pourra utiliser.
    // Bien évidemment, certains iterators peuvent être utilisés sur des collections différentes.
    public function getIterator($debugMode = false): \Iterator
    {
        return new ForwardIterator($this,$debugMode);
    }

    public function getBackwardIterator(): \Iterator
    {
        return new BackwardIterator($this);
    }
}
