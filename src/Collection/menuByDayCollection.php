<?php

namespace App\Collection;

use App\Iterators\BackwardIterator;
use App\Iterators\ForwardIterator;
use App\Iterators\ReversedRowsIterator;

class menuByDayCollection implements \IteratorAggregate, \Countable {

    private array $items;

    public function __construct($file)
    {
        $this->items = array_map('str_getcsv', file($file));
    }

    public function getItems(): array
    {
        return $this->items;
    }

    // J'implémente countable et créé la fonction, je vais en avoir besoin dans un des itérateurs que je déclare pour ma collection.
    public function count(): int
    {
        return count($this->items);
    }

    // Je déclare les itérateurs que ma collection pourra utiliser.
    // Bien évidemment, certains iterators peuvent être utilisés sur des collections différentes.
    public function getIterator(): \Iterator
    {
        return new ForwardIterator($this);
    }

    public function getBackwardIterator(): \Iterator
    {
        return new BackwardIterator($this);
    }

    // Ici, c'est un contre-exemple, on est obligé d'avoir un tableau à deux dimensions pour pouvoir l'utiliser.
    public function getReversedRowIterator($backward = false): \Iterator {
        return new ReversedRowsIterator($this, $backward);
    }
}
