<?php

namespace App\Iterators;

class ReversedRowsIterator implements \Iterator
{
    private int $position;

    public function __construct(
        private readonly \Countable $collection,
        private $backward = false
    )
    {
    }

    public function rewind(): void
    {
        $this->position = $this->backward ? $this->collection->count() - 1 : 0;
    }

    public function valid(): bool
    {
        return isset($this->collection->getItems()[$this->position])
            && is_array($this->collection->getItems()[$this->position]);
    }

    public function current(): array
    {
        return array_reverse($this->collection->getItems()[$this->position]);
    }

    public function key(): int
    {
        return $this->position;
    }

    public function next(): void
    {
        $this->position = $this->backward ? $this->position -1 : $this->position + 1;
    }
}
