<?php

namespace App\Iterators;

use Countable;

class BackwardIterator implements \Iterator
{
    private int $position;

    public function __construct(
        private readonly Countable $collection,
    )
    {
    }

    public function current(): mixed
    {
        return $this->collection->getItems()[$this->position];
    }

    public function next(): void
    {
        --$this->position;
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return isset($this->collection->getItems()[$this->position]);
    }

    public function rewind(): void
    {
        $this->position = $this->collection->count() - 1;
    }
}
