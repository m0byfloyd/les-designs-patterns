<?php

namespace App\Iterators;

// On implémente Iterator pour que PHP puisse comprendre que c'est un Iterator.
// Iterator étends Traversable, ce qui fait que nous allons pouvoir utiliser notre Iterator dans un foreach.
class ForwardIterator implements \Iterator
{
    private int $position;

    // Étape 0 : On construit l'itérateur
    public function __construct(
        private readonly iterable $collection,
    )
    {
    }

    // Étape 1 : On rembobine l'itérateur, on ramène la position de notre itérateur à la première de notre collection qui nous intéresse.
    public function rewind(): void
    {
        $this->position = 0;
    }

    // Étape 2 : On vérifie que la valeur se situant dans notre collection à la position de l'itérateur est valide.
    public function valid(): bool
    {
        return isset($this->collection->getItems()[$this->position]);
    }


    // Étape 3 : On retourne la valeur se situant à la position de l'itérateur dans notre collection.
    public function current(): mixed
    {
        return $this->collection->getItems()[$this->position];
    }

    // Étape 3.5 (facultative) : Nous permet de récupérer la clef de notre position. (c'est le $key de $key => $value)
    public function key(): int
    {
        return $this->position;
    }

    // Étape 4 : On indique à l'itérateur de passer à la prochaine valeur en incrémentant la position.
    public function next(): void
    {
        ++$this->position;
    }
}
