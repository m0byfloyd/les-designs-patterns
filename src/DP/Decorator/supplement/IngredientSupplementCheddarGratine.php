<?php

namespace App\DP\Decorator\supplement;

use App\DP\Decorator\Ingredient;

class IngredientSupplementCheddarGratine extends Ingredient
{
    public function eat(): string
    {
        return parent::eat() . 'supplément cheddar gratiné ';
    }

}
