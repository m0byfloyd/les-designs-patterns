<?php

namespace App\DP\Decorator\supplement;

use App\DP\Decorator\Ingredient;

class IngredientSupplementBoursin extends Ingredient
{
    public function eat(): string
    {
        return parent::eat() . 'supplément boursin ';
    }
}
