<?php

namespace App\DP\Decorator;

class Taco implements TacoInterface
{
    public function __construct()
    {
    }

    public function eat(): string
    {
        return 'Hmm miam miam je mange un bon Taco ';
    }
}
