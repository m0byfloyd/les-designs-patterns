<?php

namespace App\DP\Decorator;

interface TacoInterface
{
    public function eat(): string;
}
