<?php

namespace App\DP\Decorator\sauce;

use App\DP\Decorator\Ingredient;

class IngredientSauceAlgerienne extends Ingredient
{
    public function eat(): string
    {
        return parent::eat() . 'sauce Algérienne ';
    }

}
