<?php

namespace App\DP\Decorator\sauce;

use App\DP\Decorator\Ingredient;

class IngredientSauceBarbecue extends Ingredient
{
    public function eat(): string
    {
        return parent::eat() . 'sauce Algérienne ';
    }

}
