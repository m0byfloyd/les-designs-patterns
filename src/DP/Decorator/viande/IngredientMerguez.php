<?php

namespace App\DP\Decorator\viande;

use App\DP\Decorator\Ingredient;

class IngredientMerguez extends Ingredient
{
    public function eat(): string
    {
        return parent::eat() . 'Merguez ';
    }
}
