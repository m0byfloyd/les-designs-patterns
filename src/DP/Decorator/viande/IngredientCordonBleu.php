<?php

namespace App\DP\Decorator\viande;

use App\DP\Decorator\Ingredient;

class IngredientCordonBleu extends Ingredient
{
    public function eat(): string
    {
        return parent::eat() . 'cordon bleu ';
    }

}
