<?php

namespace App\DP\Decorator\viande;

use App\DP\Decorator\Ingredient;

class IngredientViandeHachee extends Ingredient
{
    public function eat(): string
    {
        return parent::eat() . 'boursin ';
    }

}
