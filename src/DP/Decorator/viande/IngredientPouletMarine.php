<?php

namespace App\DP\Decorator\viande;

use App\DP\Decorator\Ingredient;

class IngredientPouletMarine extends Ingredient
{
    public function eat(): string
    {
        return parent::eat() . 'Poulet Mariné ';
    }

}
