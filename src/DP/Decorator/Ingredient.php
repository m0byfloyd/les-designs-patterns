<?php

namespace App\DP\Decorator;

abstract class Ingredient implements TacoInterface
{
    public function __construct(
        private readonly TacoInterface $tacos,
    ){}

    public function eat(): string
    {
        return $this->tacos->eat();
    }

}
